S-expressions
=============

The syntax of Stardust is rooted in *S-expressions*. S-expressions consist of
literal representations of various data structures, including atoms, Booleans,
text values, lists, and dictionaries (or *dicts* for short). S-expressions are
sufficiently expressive to encode any form of data, covering both programs and
the data those programs manipulate. Examples of S-expressions::

    ;; Atoms
    delete-file
    message
    >>=

    ;; Booleans
    #t #f

    ;; Text values
    "Hello, world!"
    "Content-Type: text/html\r\n"
    ""

    ;; Lists
    (delete-file fs "temporary")
    (nested (lists) and (more (lists)))
    ()

    ;; Dicts
    {name "Chloe", favorite-color "purple"}
    {#t "true", #f "false"}
    {}

Whitespace
----------

Spaces, newlines, and commas are considered whitespace. Whitespace is ignored:
you can put it wherever you like (except within a token) and it will make no
difference to the S-expression parser.

A semicolon introduces a *comment*. Comments extend until the end of the line
they begin on. Like whitespace, comments are ignored. Comments are often used
for explaining code or temporarily disabling it.

Data structures
---------------

Each class of S-expressions is explained separately in the following
subsections. Different programs may interpret them in different ways, and these
interpretations are documented in other chapters. This section is just about
the data structures themselves, and how they are written in their literal
forms.

Atoms
~~~~~

An atom is a constant with a name. It has no further information and cannot be
subdivided. Atoms are typically used for names in programs and keys in dicts.
Atoms are written by their name, which can consist of the following characters,
but may not begin with a digit::

    a-z A-Z 0-9
    + - * / %
    < = >
    . ? !

Booleans
~~~~~~~~

A Boolean, named after logician George Boole, is a value that is one of two
values: true or false. They are written as ``#t`` and ``#f`` respectively.
Booleans are often used when a choice has to be made during the execution of a
program.

Text values
~~~~~~~~~~~

Text values contain arbitrary Unicode text. They are delimited by double
quotes. Backslashes can be used to escape certain special characters.

Lists
~~~~~

Lists are finite, ordered sequences of other S-expressions. Lists are written
by enclosing zero or more whitespace-separated S-expressions within
parentheses.

Lists can also be written by prefixing a sequence of S-expressions by ``##``.
These lists will extend as far as they can, which is usually until the next
closing parenthesis or the end of the file. For example::

    ; These S-expressions are identical.
    (not ## = a b)
    (not (= a b))

Dicts
~~~~~

Dictionaries, or *dicts* for short, are finite, unordered mappings from unique
keys to values. Keys and values are juxtaposed, along with other entries, and
enclosed inside braces.
