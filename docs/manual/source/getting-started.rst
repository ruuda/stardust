Getting started
===============

.. highlight:: bash

To get started using Stardust, you first have to install it. Stardust is in
early development and there is not yet a release cycle, so you will have to
grab the current source code from the repository::

    git clone https://gitlab.com/kloe/stardust.git
    cd stardust

The compiler and standard library use the Bazel_ build system. The compiler is
written in OCaml_. After installing Bazel and OCaml, you can execute the
following command to compile the Stardust compiler::

    bazel build //compiler/...

This guide also recommends you use Bazel for building your Stardust code, but
you don't have to. The Stardust repository contains Bazel extensions for
building Stardust code.

.. _Bazel: https://bazel.build
.. _Ocaml: https://ocaml.org

Running a program
-----------------

If all went well, you can now invoke the compiler manually. The following
sequence of commands will compile a simple hello world program to ECMAScript.
ECMAScript is currently the only supported target language, meaning your
Stardust code will be converted into ECMAScript code ready for execution.

::

    echo '<script>'
    cat rt/ecmascript/rt.js
    bazel-bin/compiler/sdc --target=ecmascript <<EOF
      (defforeign log (msg)
        {ecmascript "console.log(msg);\nvar $out = null;\n"})
      (defsub main ()
        (log "Hello, world!"))
    EOF
    echo 'window.stardust.rt.finalizeInitialization();'
    echo 'window.stardust["main~0"]();'
    echo '</script>'

If you write the output of this sequence of commands to a HTML file and open it
in a web browser, it should log a message to the console.

Running the test suite
----------------------

Stardust ships with a test suite for the standard library. The test suite uses
QUnit_, but Bazel will automatically download that for you. To build the test
suite with Bazel and run it in your web browser::

    bazel build //std/test/...
    xdg-open bazel-genfiles/std/test/test.html

.. _QUnit: https://qunitjs.com

Building your programs
----------------------

Assuming you use Bazel for building your programs, you can use the Stardust
repository as a Bazel repository by adding it to your workspace. Add the
following to your WORKSPACE file::

    http_archive(
        name = "stardust",
        url = "https://gitlab.com/kloe/stardust/-/archive/master/stardust-master.tar.gz",
        strip_prefix = "stardust-master",
    )

You can then put the following in your BUILD file to generate an ECMAScript
source file of your Stardust sources. The order of the files doesn't matter
since in Stardust you can call subroutines that are defined later.

.. code-block:: skylark

    load(
        "@stardust//tools:stardust.bzl",
        "stardust_ecmascript_library",
        "stardust_ecmascript_binary",
    )

    stardust_ecmascript_library(
        name = "ecmascript",
        srcs = [":myfile1.sd", ":myfile2.sd"],
    )

    stardust_ecmascript_binary(
        name = "ecmascript_binary",
        srcs = [
            ":ecmascript",
            "@stardust//std/src:ecmascript",
        ],
        main = "main~0",
    )

Now you can build your Stardust code with Bazel::

    bazel build //...
    cat bazel-bin/ecmascript_binary.js
