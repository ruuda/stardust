Stardust
========

Stardust is a general-purpose programming language built on a simple foundation
and features a consistent standard library with no heuristics or implicit
conversions. Stardust has no support for global state or monkey patching,
making it a great fit for maintainable software. Moreover, Stardust embraces
dependency injection and offers built-in tracing functionality to aid in
analyzing runtime failures in production environments.

.. toctree::
   :maxdepth: 2

   getting-started
   s-expressions
   compilation
