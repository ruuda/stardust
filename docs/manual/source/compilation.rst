Compilation
===========

Compilation is the process by which S-expressions are read and interpreted as a
program, which is in turn analyzed and turned into code in a target language.

At the top level is a sequence of S-expressions denoting *definitions*. Certain
definitions may contain *expressions*, which are executable code. Unlike some
programming languages, but much like others, expressions cannot occur at the
top level. This rigidity allows for a greater degree of static analysis of the
program.

Note that compiling the program and running the program are completely separate
processes. You cannot compile S-expressions at runtime and evaluate them.
Allowing this would limit the power of static analysis tooling, with only a
marginal (and questionable) gain in flexibility.

Translation units
-----------------

A translation unit is a sequence of definitions, encoded as S-expressions. The
compiler processes one translation unit at a time, read from stdin. Translation
units may be empty, but this is of limited use.

Definitions
-----------

There are various types of definitions. Syntactically, *using* and *namespace*
are treated as definitions, although they do not define anything. They do,
however, contain other definitions.

using
~~~~~

A *using* definition is not really a definition, but it provides a context for
definitions nested inside of it. A using definition has the following syntax::

    (using <namespace> (<names> ...)
      <definitions> ...)

When the compiler encounters a using definition, it remembers the given names
as if they were in the given namespace for the remainder of the using
definition. To illustrate this, we can look at the following example of two
equivalent translation units::

    (defsub twice (x)
      (std/lists/cons x (std/lists/cons x (std/lists/nil))))

This translation unit also be written as follows::

    ## using std/lists (cons nil)

    (defsub twice (x)
      (cons x (cons x (nil))))

namespace
~~~~~~~~~

A *namespace* definition is not really a definition, but it provides a context
for definitions nested inside of it. A namespace definition has the following
syntax::

    (namespace <namespace>
      <definitions> ...)

Whenever the definitions inside the namespace definition define something, they
define it under the given namespace. Using namespaces, multiple libraries can
define subroutines that have the same names. For example, there is no naming
conflict in the following translation unit, even though the two subroutines
have the same unqualified names::

    (namespace lists
      (defsub display (xs)
        ...))

    (namespace dicts
      (defsub display (xs)
        ...))

To use a subroutine defined in a different namespace, prefix it with its
namespace and a forward slash::

    (defsub example ()
      (dicts/display {'type 'example, 'message "hello"}))

Expressions
-----------
