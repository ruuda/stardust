"use strict";

window.stardust = {};

(function($$, $) {
    $.rt = {};



    function assert(name, cond) {
        if (typeof cond !== "boolean") {
            throw Error("Assertion condition must be Boolean");
        }
        if (!cond) {
            throw Error("Assertion failed: " + name);
        }
    }

    function hashString(s) {
        var hash = 0;
        for (var i = 0, n = s.length; i < n; ++i) {
            var char = s.charCodeAt(i);
            hash = (hash * 31 | 0) + char | 0;
        }
        return hash;
    };



    function Atom(name) {
        this.name = name;
    }



    function List() {
    }

    List.foldl = function(xs, z, f) {
        while (xs instanceof Cons) {
            z = f(z, xs.head);
            xs = xs.tail;
        }
        return z;
    };

    List.zipWith = function(xs, ys, f) {
        var array = [];
        while (xs instanceof Cons && ys instanceof Cons) {
            var element = f(xs.head, ys.head);
            array.push(element);
            xs = xs.tail;
            ys = ys.tail;
        }

        var out = new Nil();
        for (var i = array.length - 1; i >= 0; --i) {
            out = new Cons(array[i], out);
        }

        return out;
    };

    List["="] = function(xs, ys) {
        for (;;) {
            if (xs instanceof Cons && ys instanceof Cons) {
                if (!$["std/=~2"](xs.head, ys.head)) {
                    return false;
                }
                xs = xs.tail;
                ys = ys.tail;
            } else if (xs instanceof Nil && ys instanceof Nil) {
                return true;
            } else {
                return false;
            }
        }
    };

    function Cons(head, tail) {
        this.head = head;
        this.tail = tail;
    }
    Cons.prototype = Object.create(List.prototype);

    function Nil() {
    }
    Nil.prototype = Object.create(List.prototype);



    // Dicts are implemented as unbalanced binary search trees. The keys used
    // in the trees are the hashes of the dict keys. The values in the trees
    // are arrays of collisions. Usually the arrays are singleton.
    //
    // TODO: Use a more efficient implementation.

    function Dict() {
    }

    function DictEntry(key, value) {
        this.key   = key;
        this.value = value;
    }

    function DictEmpty() {
    }
    DictEmpty.prototype = Object.create(Dict.prototype);

    DictEmpty.prototype.insert = function(k, v) {
        var h = $["std/hash~1"](k);
        return new DictBranch(this, h, [new DictEntry(k, v)], this);
    };

    DictEmpty.prototype.member = function(k) {
        return false;
    };

    DictEmpty.prototype.lookup = function(k) {
        return null;
    };

    function DictBranch(less, hash, bucket, greater) {
        this.less    = less;
        this.hash    = hash;
        this.bucket  = bucket;
        this.greater = greater;
    }
    DictBranch.prototype = Object.create(Dict.prototype);

    DictBranch.prototype.insert = function(k, v) {
        var h = $["std/hash~1"](k);
        if (h < this.hash) {
            var less = this.less.insert(k, v);
            return new DictBranch(less, this.hash, this.bucket, this.greater);
        } else if (h === this.hash) {
            var bucket = this.bucket.slice();
            for (var i = 0, n = bucket.length; i < n; ++i) {
                var entry = bucket[i];
                if ($["std/=~2"](k, entry.key)) {
                    bucket[i] = new DictEntry(k, v);
                    break;
                }
            }
            if (i === n) {
                bucket.push(new DictEntry(k, v));
            }
            return new DictBranch(this.less, this.hash, bucket, this.greater);
        } else {
            var greater = this.greater.insert(k, v);
            return new DictBranch(this.less, this.hash, this.bucket, greater);
        }
    };

    DictBranch.prototype.member = function(k) {
        var h = $["std/hash~1"](k);
        if (h < this.hash) {
            return this.less.member(k);
        } else if (h === this.hash) {
            var bucket = this.bucket;
            for (var i = 0, n = bucket.length; i < n; ++i) {
                var entry = bucket[i];
                if ($["std/=~2"](k, entry.key)) {
                    return true;
                }
            }
            return false;
        } else {
            return this.greater.member(k);
        }
    };

    DictBranch.prototype.lookup = function(k) {
        var h = $["std/hash~1"](k);
        if (h < this.hash) {
            return this.less.lookup(k);
        } else if (h === this.hash) {
            var bucket = this.bucket;
            for (var i = 0, n = bucket.length; i < n; ++i) {
                var entry = bucket[i];
                if ($["std/=~2"](k, entry.key)) {
                    return entry.value;
                }
            }
            return null;
        } else {
            return this.greater.lookup(k);
        }
    };



    var definedMethods = new Set();
    var pendingMethodImplementations = new Map();

    function defineMethod(name) {
        definedMethods.add(name);
        if (pendingMethodImplementations.has(name)) {
            var pendings = pendingMethodImplementations.get(name);
            for (var i = 0, npendings = pendings.length; i < npendings; ++i) {
                var pending = pendings[i];
                addMethodImplementation(name, pending.cls, pending.impl);
            }
            pendingMethodImplementations.delete(name);
        }
        return function() {
            var self = arguments[0];
            return self[name].apply(null, arguments);
        };
    }

    function implementMethod(name, cls, impl) {
        if (definedMethods.has(name)) {
            addMethodImplementation(name, cls, impl);
        } else {
            if (!pendingMethodImplementations.has(name)) {
                pendingMethodImplementations.set(name, []);
            }
            var pending = {cls: cls, impl: impl};
            pendingMethodImplementations.get(name).push(pending);
        }
    }

    function addMethodImplementation(name, cls, impl) {
        var proto;
        switch (cls) {
            case 'std/atom': proto = Atom.prototype;    break;
            case 'std/bool': proto = Boolean.prototype; break;
            case 'std/list': proto = List.prototype;    break;
            case 'std/text': proto = String.prototype;  break;
            default: throw Error("Unknown class: " + cls);
        }
        Object.defineProperty(proto, name, {value: impl});
    }



    /**
     * Set of names of subroutines that are called somewhere. Initialization
     * finalization will check that these have all been defined, so that no
     * runtime errors will occur because of calls to undefined subroutines.
     */
    var callees = new Set();

    /**
     * Calls to this function are generated by the compiler at the end of each
     * translation unit.
     */
    function registerCallee(callee) {
        callees.add(callee);
    }

    function finalizeInitialization() {
        if (pendingMethodImplementations.size !== 0) {
            throw Error("There are pending method implementations");
        }

        for (var it = callees.values();;) {
            var entry = it.next();
            if (entry.done) { break; }
            if (!$.hasOwnProperty(entry.value)) {
                throw Error("Callee was not defined: " + entry.value);
            }
        }
    }



    $.rt.assert = assert;
    $.rt.hashString = hashString;

    $.rt.Atom = Atom;

    $.rt.List = List;
    $.rt.Cons = Cons;
    $.rt.Nil = Nil;

    $.rt.Dict = Dict;
    $.rt.DictEmpty = DictEmpty;
    $.rt.DictBranch = DictBranch;

    $.rt.defineMethod = defineMethod;
    $.rt.implementMethod = implementMethod;

    $.rt.registerCallee = registerCallee;
    $.rt.finalizeInitialization = finalizeInitialization;
})(window, window.stardust);
