; NOTE: The following subroutines must be defined using defforeign: empty,
;       insert. Applications of these subroutines are generated when using
;       quoting and dict literals.

## using std (bool? dict?)
## using std/dicts (member?)

## namespace std

(defforeign dict? (x)
  {ensure {"bool?" (bool? (out))}}
  {ecmascript "var $out = x instanceof $.rt.Dict;\n"})

## namespace dicts

(defforeign empty ()
  {ensure {"dict?" (dict? (out))}}
  {ecmascript "var $out = new $.rt.DictEmpty();\n"})

(defforeign insert (d k v)
  {require {"dict?" (dict? d)}
   ensure  {"dict?" (dict? (out))}}
  {ecmascript "var $out = d.insert(k, v);\n"})

(docarticle "std/dicts/member?~2"
  (p (code "(member? d k)") " evaluates to a Boolean indicating whether "
      "the key " (code "k") " exists in the dict " (code "d") "."))

(defforeign member? (d k)
  {require {"dict?" (dict? d)}
   ensure  {"bool?" (bool? (out))}}
  {ecmascript "var $out = d.member(k);\n"})

(docarticle "std/dicts/lookup!~2"
  (p (code "(lookup! d k)") " looks up the value corresponding to the key "
      (code "k") " in the dict " (code "d") ". The preconditions include "
      "an assertion that the key is present in the dict."))

(defforeign lookup! (d k)
  {require {"dict?"   (dict? d)
            "member?" (member? d k)}}
  {ecmascript "var $out = d.lookup(k);\n"})
