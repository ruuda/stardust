def _stardust_library(ctx, out, target):
    srcs = ctx.files.srcs
    sdc = ctx.executable.sdc

    intermediates = []
    for src in srcs:
        intermediate = ctx.new_file(src.path + "." + target)
        ctx.actions.run_shell(
            inputs = [src, sdc],
            outputs = [intermediate],
            mnemonic = "sdc",
            command = """
                set -o errexit
                "$2" --target="$4" < "$1" > "$3"
            """,
            arguments = [src.path, sdc.path, intermediate.path, target],
        )
        intermediates.append(intermediate)

    ctx.actions.run_shell(
        inputs = intermediates,
        outputs = [out],
        mnemonic = "cat",
        command = """
            set -o errexit
            cat "${@:2}" > "$1"
        """,
        arguments = [out.path] + [imd.path for imd in intermediates],
    )

def _stardust_ecmascript_library(ctx):
    return _stardust_library(ctx, ctx.outputs.js, "ecmascript")

def _stardust_html_library(ctx):
    return _stardust_library(ctx, ctx.outputs.html, "html")

stardust_ecmascript_library = rule(
    implementation = _stardust_ecmascript_library,
    attrs = {
        "srcs": attr.label_list(allow_files = True),
        "sdc": attr.label(executable = True, cfg = "host", default = "//compiler:sdc"),
    },
    outputs = {
        "js": "%{name}.js",
    },
)

stardust_html_library = rule(
    implementation = _stardust_html_library,
    attrs = {
        "srcs": attr.label_list(allow_files = True),
        "sdc": attr.label(executable = True, cfg = "host", default = "//compiler:sdc"),
    },
    outputs = {
        "html": "%{name}.html",
    },
)

def _stardust_ecmascript_binary(ctx):
    rt   = ctx.files.rt[0]
    srcs = ctx.files.srcs
    main = ctx.attr.main
    out  = ctx.outputs.js
    ctx.actions.run_shell(
        inputs = [rt] + srcs,
        outputs = [out],
        mnemonic = "link",
        command = """
            (
                cat "$2"
                cat "${@:4}"
                echo 'window.stardust.rt.finalizeInitialization();'
                echo 'window.stardust["'"$3"'"]();'
            ) > "$1"
        """,
        arguments = [out.path, rt.path, main] + [src.path for src in srcs],
    )

stardust_ecmascript_binary = rule(
    implementation = _stardust_ecmascript_binary,
    attrs = {
        "srcs": attr.label_list(allow_files = True),
        "main": attr.string(mandatory = True),
        "rt": attr.label(allow_files = True, default = "//rt/ecmascript:rt.js"),
    },
    outputs = {
        "js": "%{name}.js",
    },
)
