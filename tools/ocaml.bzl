def _ocaml_lex(ctx):
    src = ctx.files.src[0]
    ml = ctx.outputs.ml
    ctx.actions.run(
        inputs = [src],
        outputs = [ml],
        mnemonic = "ocamllex",
        executable = "ocamllex",
        arguments = ["-o", ml.path, src.path],
    )

def _ocaml_yacc(ctx):
    src = ctx.files.src[0]
    ml = ctx.outputs.ml
    mli = ctx.outputs.mli
    ctx.actions.run_shell(
        inputs = [src],
        outputs = [ml, mli],
        mnemonic = "ocamlyacc",
        command = """
            set -o errexit
            ocamlyacc --strict -v "$1" || (cat "${1%.mly}.output"; false)
            mv "${1%.mly}.ml"  "$2"
            mv "${1%.mly}.mli" "$3"
        """,
        arguments = [src.path, ml.path, mli.path],
    )

def _ocaml_module(ctx):
    src = ctx.files.src[0]
    mlis = ctx.files.mli
    deps = ctx.files.deps
    cmi = ctx.outputs.cmi
    cmx = ctx.outputs.cmx
    o = ctx.outputs.o
    ctx.actions.run_shell(
        inputs = [src] + mlis + deps,
        outputs = [cmi, cmx, o],
        mnemonic = "ocamlopt",
        command = """
            set -o errexit

            # Move dependencies so ocamlopt can find them.
            for f in "${@:6}"; do
                mv "$f" .
            done

            # Unfortunately, ocamlopt cares about file names.
            MODULE_NAME="$(basename "${3%.cmi}")"
            mv "$1" "$MODULE_NAME.ml"
            if [ -n "$2" ]; then
                mv "$2" "$MODULE_NAME.mli"
            fi

            # Compile mli file if provided.
            if [ -n "$2" ]; then
                ocamlopt "$MODULE_NAME.mli"
            fi

            # Compile ml file.
            ocamlopt -c "$MODULE_NAME.ml"

            # Move outputs so Bazel can find them.
            mv "$MODULE_NAME.cmi" "$3"
            mv "$MODULE_NAME.cmx" "$4"
            mv "$MODULE_NAME.o"   "$5"
        """,
        arguments = [
            src.path,
            "".join([mli.path for mli in mlis]),
            cmi.path,
            cmx.path,
            o.path,
        ] + [dep.path for dep in deps],
    )

def _ocaml_binary(ctx):
    srcs = ctx.files.srcs
    out = ctx.outputs.out
    ctx.actions.run(
        inputs = srcs,
        outputs = [out],
        mnemonic = "ocamlopt",
        executable = "ocamlopt",
        arguments =
            ["-o", out.path] +
            [src.path for src in srcs if src.extension == "cmx"],
    )
    return [DefaultInfo(executable = out)]

ocaml_lex = rule(
    implementation = _ocaml_lex,
    attrs = {
        "src": attr.label(allow_files = True, mandatory = True),
    },
    outputs = {
        "ml": "%{name}.ml",
    },
)

ocaml_yacc = rule(
    implementation = _ocaml_yacc,
    attrs = {
        "src": attr.label(allow_files = True, mandatory = True),
    },
    outputs = {
        "ml": "%{name}.ml",
        "mli": "%{name}.mli",
    },
)

ocaml_module = rule(
    implementation = _ocaml_module,
    attrs = {
        "src": attr.label(allow_files = True, mandatory = True),
        "mli": attr.label(allow_files = True),
        "deps": attr.label_list(),
    },
    outputs = {
        "cmi": "%{name}.cmi",
        "cmx": "%{name}.cmx",
        "o": "%{name}.o",
    },
)

ocaml_binary = rule(
    implementation = _ocaml_binary,
    executable = True,
    attrs = {
        "srcs": attr.label_list(allow_empty = False),
    },
    outputs = {
        "out": "%{name}",
    },
)
