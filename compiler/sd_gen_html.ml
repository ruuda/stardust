module I = Sd_io
module S = Sd_syntax

let rec gen_xml w = function
  | S.Xml_text text ->
      (* TODO: Escape & and <. *)
      I.write_all w text

  | S.Xml_element (tag, elems) ->
      begin
        I.write_all w "<";
        I.write_all w tag;
        I.write_all w ">";
        List.iter (gen_xml w) elems;
        I.write_all w "</";
        I.write_all w tag;
        I.write_all w ">";
      end

let rec gen_definition w = function
  | S.Using_definition (_, _, ds) ->
      List.iter (gen_definition w) ds

  | S.Namespace (_, ds) ->
      List.iter (gen_definition w) ds

  | S.Defsub (_, _, _, _) ->
      ()

  | S.Defforeign (_, _, _, _) ->
      ()

  | S.Defmeth (_, _) ->
      ()

  | S.Defimpl (_, _, _, _) ->
      ()

  | S.Docarticle (title, elems) ->
      let h1 = S.Xml_element ("h1", [S.Xml_text title]) in
      let article = S.Xml_element ("article", h1 :: elems) in
      gen_xml w article

let gen_translation_unit w =
  List.iter (gen_definition w)
