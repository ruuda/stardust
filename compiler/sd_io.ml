type writer = <
  write : bytes -> int -> int -> unit
>

let write_all w b =
  w#write b 0 (Bytes.length b)

let writer_of_out_channel c =
  object
    method write = output c
  end
