module N = Sd_name
module S = Sd_syntax

module Callee_set = struct
  include Set.Make (struct
    type t = int * N.qglobal
    let compare = Pervasives.compare
  end)

  let unions = List.fold_left union empty
end

let (@@@) = Callee_set.union

let rec collect_translation_unit tu =
  Callee_set.unions @@ List.map collect_definition tu

and collect_definition = function
  | S.Using_definition (_, _, ds) ->
      Callee_set.unions @@ List.map collect_definition ds

  | S.Namespace (_, ds) ->
      Callee_set.unions @@ List.map collect_definition ds

  | S.Defsub (_, _, contract, body) ->
      collect_contract contract @@@
      collect_expression body

  | S.Defforeign (_, _, contract, _) ->
      collect_contract contract

  | S.Defmeth (_, _) ->
      Callee_set.empty

  | S.Defimpl (_, _, _, body) ->
      collect_expression body

  | S.Docarticle (_, _) ->
      Callee_set.empty

and collect_contract contract =
  let conds = contract.S.requires @ contract.S.ensures in
  Callee_set.unions @@ List.map collect_expression @@ List.map snd conds

and collect_expression = function
  | S.Using_expression (_, _, expr) ->
      collect_expression expr

  | S.Variable _ ->
      Callee_set.empty

  | S.Literal_bool _ ->
      Callee_set.empty

  | S.Literal_text _ ->
      Callee_set.empty

  | S.Apply (callee, args) ->
      Callee_set.add (List.length args, callee) @@
        Callee_set.unions @@ List.map collect_expression args

  | S.Call (callee, args) ->
      Callee_set.unions @@
        List.map collect_expression (callee :: args)

  | S.Closure (_, body) ->
      collect_expression body

  | S.If (cond, if_true, if_false) ->
      collect_expression cond @@@
      collect_expression if_true @@@
      collect_expression if_false

  | S.Out ->
      Callee_set.empty
