module C = Sd_collect_callees
module G = Sd_gen_ecmascript
module H = Sd_gen_html
module I = Sd_io
module L = Sd_lex
module P = Sd_parse
module Q = Sd_qualify
module R = Sd_recognize

exception Invalid_usage

let () =
  let lexbuf  = Lexing.from_channel stdin in
  let sexps   = P.sexps_eof L.token lexbuf in
  let tu      = R.recognize_translation_unit sexps in
  let qtu     = Q.qualify_translation_unit tu in
  let callees = C.collect_translation_unit qtu in
  match Sys.argv with
  | [| _; "--target=ecmascript" |] ->
      let es = G.gen_translation_unit qtu callees in
      let w  = I.writer_of_out_channel stdout in
      List.iter (G.Ecmascript.format_statement w) es

  | [| _; "--target=html" |] ->
      let w = I.writer_of_out_channel stdout in
      H.gen_translation_unit w qtu

  | _ ->
      raise Invalid_usage
