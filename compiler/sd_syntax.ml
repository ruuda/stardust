(**
 * Data structures for abstract syntax trees.
 *
 * Types are parameterized over type of global, which may be
 * {!type:Sd_name.qglobal} or {!type:Sd_name.uglobal}.
 *)

module T = Sd_target

(** A translation unit is a list of definitions. *)
type 'g translation_unit = 'g definition list

(** Definitions occur at the top level. They define things. *)
and 'g definition =
  | Using_definition of string list * string list * 'g definition list
  | Namespace of string * 'g definition list
  | Defsub of string * string list * 'g contract * 'g expression
  | Defforeign of string * string list * 'g contract * (T.t * string) list
  | Defmeth of string * string list
  | Defimpl of 'g * 'g * string list * 'g expression
  | Docarticle of string * xml list

and 'g contract = { requires: (string * 'g expression) list
                  ; ensures:  (string * 'g expression) list }

(**
 * Expressions occur within subroutine definitions and other expressions. They
 * are evaluated to produce values and side-effects.
 *)
and 'g expression =
  | Using_expression of string list * string list * 'g expression
  | Variable of string
  | Literal_bool of bool
  | Literal_text of string
  | Apply of 'g * 'g expression list
  | Call of 'g expression * 'g expression list
  | Closure of string list * 'g expression
  | If of 'g expression * 'g expression * 'g expression
  | Out

(**
 * Used for documentation.
 *)
and xml =
  | Xml_text of string
  | Xml_element of string * xml list
