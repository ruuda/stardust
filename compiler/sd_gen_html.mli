(** Generate HTML from documentation. *)

val gen_translation_unit
  :  Sd_io.writer
  -> 'q Sd_syntax.translation_unit
  -> unit

val gen_definition
  :  Sd_io.writer
  -> 'q Sd_syntax.definition
  -> unit
