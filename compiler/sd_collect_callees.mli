(** Callee collection is used for callee registration. *)

module Callee_set : Set.S
  with type elt = int * Sd_name.qglobal

val collect_translation_unit
  :  Sd_name.qglobal Sd_syntax.translation_unit
  -> Callee_set.t

val collect_definition
  :  Sd_name.qglobal Sd_syntax.definition
  -> Callee_set.t

val collect_contract
  :  Sd_name.qglobal Sd_syntax.contract
  -> Callee_set.t

val collect_expression
  :  Sd_name.qglobal Sd_syntax.expression
  -> Callee_set.t
