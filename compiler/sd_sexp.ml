(** S-expressions. *)

type t =
  | Atom of string
  | Bool of bool
  | Text of string
  | List of t list
  | Dict of (t * t) list
