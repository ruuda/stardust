module S = Sd_syntax
module T = Sd_target
module X = Sd_sexp

exception Unrecognized_definition
exception Unrecognized_expression

let recognize_global name =
  match List.rev @@ String.split_on_char '/' name with
  | [single] ->
      `Unqualified name
  | (name :: rev_namespace) ->
      `Qualified (List.rev rev_namespace, name)
  | _ ->
      (* String.split_on_char never returns nil. *)
      raise Exit

let recognize_params exc =
    List.map @@ function
      | X.Atom name -> name
      | _ -> raise exc

let rec recognize_xml exc = function
  | X.Atom _ ->
      raise exc

  | X.Bool bool ->
      raise exc

  | X.Text text ->
      S.Xml_text text

  | X.List (X.Atom tag :: elems) ->
      let elems' = List.map (recognize_xml exc) elems in
      S.Xml_element (tag, elems')

  | X.List _ ->
      raise exc

  | X.Dict _ ->
      raise exc

let rec recognize_translation_unit es =
  List.map recognize_definition es

and recognize_definition = function
  | X.List (X.Atom "using" :: X.Atom ns :: X.List names :: ds) ->
      let (ns', names') = recognize_using ns names         in
      let ds'           = List.map recognize_definition ds in
      S.Using_definition (ns', names', ds')

  | X.List (X.Atom "namespace" :: X.Atom name :: ds) ->
      let name' = String.split_on_char '/' name in
      let ds' = List.map recognize_definition ds in
      List.hd @@ List.fold_right (fun n d -> [S.Namespace (n, d)]) name' ds'

  | X.List [X.Atom "defsub"; X.Atom name; X.List params; body] ->
      recognize_definition @@ X.List [ X.Atom "defsub"
                                     ; X.Atom name
                                     ; X.List params
                                     ; X.Dict []
                                     ; body ]

  | X.List [X.Atom "defsub"; X.Atom name; X.List params; X.Dict contract; body] ->
      let params' = recognize_params Unrecognized_definition params in
      let contract' = recognize_contract contract in
      let body' = recognize_expression body in
      S.Defsub (name, params', contract', body')

  | X.List [X.Atom "defforeign"; X.Atom name; X.List params; X.Dict impls] ->
      recognize_definition @@ X.List [ X.Atom "defforeign"
                                     ; X.Atom name
                                     ; X.List params
                                     ; X.Dict []
                                     ; X.Dict impls ]

  | X.List [X.Atom "defforeign"; X.Atom name; X.List params; X.Dict contract; X.Dict impls] ->
      let recognize_impl = function
        | (X.Atom "ecmascript", X.Text source) -> (T.Ecmascript, source)
        | _ -> raise Unrecognized_definition
      in

      let params' = recognize_params Unrecognized_definition params in
      let contract' = recognize_contract contract in
      let impls' = List.map recognize_impl impls in
      S.Defforeign (name, params', contract', impls')

  | X.List [X.Atom "defmeth"; X.Atom name; X.List (_ :: _ as params)] ->
      let params' = recognize_params Unrecognized_definition params in
      S.Defmeth (name, params')

  | X.List [ X.Atom "defimpl"; X.Atom meth; X.Atom cls
           ; X.List (_ :: _ as params); body ] ->
      let meth' = recognize_global meth in
      let cls' = recognize_global cls in
      let params' = recognize_params Unrecognized_definition params in
      let body' = recognize_expression body in
      S.Defimpl (meth', cls', params', body')

  | X.List (X.Atom "docarticle" :: X.Text title :: elems) ->
      let elems' = List.map (recognize_xml Unrecognized_definition) elems in
      S.Docarticle (title, elems')

  | _ -> raise Unrecognized_definition

and recognize_contract =
  let recognize_condition = function
    | (X.Text name, cond) -> (name, recognize_expression cond)
    | _ -> raise Unrecognized_definition
  in

  let rec go acc = function
    | [] ->
        acc

    | (X.Atom "require", X.Dict conds) :: rest ->
        let conds' = List.map recognize_condition conds in
        let acc'   = { S.requires = conds' @ acc.S.requires
                     ; S.ensures  = acc.S.ensures } in
        go acc' rest

    | (X.Atom "ensure", X.Dict conds) :: rest ->
        let conds' = List.map recognize_condition conds in
        let acc'   = { S.requires = acc.S.requires
                     ; S.ensures  = conds' @ acc.S.ensures } in
        go acc' rest

    | _ ->
        raise Unrecognized_definition
  in

  fun pairs -> go { S.requires = []; S.ensures = [] } pairs

and recognize_expression = function
  | X.Atom name ->
      S.Variable name

  | X.Bool bool ->
      S.Literal_bool bool

  | X.Text text ->
      S.Literal_text text

  | X.List (X.Atom "and*" :: ands) ->
      let and_ elt acc = S.If (elt, acc, S.Literal_bool false) in
      let ands' = List.map recognize_expression ands in
      List.fold_right and_ ands' (S.Literal_bool true)

  | X.List (X.Atom "call" :: call) ->
      begin
        match call with
        | callee :: args ->
            let callee' = recognize_expression callee in
            let args' = List.map recognize_expression args in
            S.Call (callee', args')
        | _ ->
            raise Unrecognized_expression
      end

  | X.List (X.Atom "closure" :: closure) ->
      begin
        match closure with
        | [X.List params; body] ->
            let params' = recognize_params Unrecognized_expression params in
            let body' = recognize_expression body in
            S.Closure (params', body')
        | _ ->
            raise Unrecognized_expression
      end

  | X.List (X.Atom "if" :: if_) ->
      begin
        match if_ with
        | [cond; if_true; if_false] ->
            let cond'     = recognize_expression cond in
            let if_true'  = recognize_expression if_true in
            let if_false' = recognize_expression if_false in
            S.If (cond', if_true', if_false')
        | _ ->
            raise Unrecognized_expression
      end

  | X.List (X.Atom "let" :: let_) ->
      begin
        match let_ with
        | [X.Atom var; init; body] ->
            let init' = recognize_expression init in
            let body' = recognize_expression body in
            S.Call (S.Closure ([var], body'), [init'])
        | _ ->
            raise Unrecognized_expression
      end

  | X.List (X.Atom "or*" :: ors) ->
      let or_ elt acc = S.If (elt, S.Literal_bool true, acc) in
      let ors' = List.map recognize_expression ors in
      List.fold_right or_ ors' (S.Literal_bool false)

  | X.List (X.Atom "out" :: out) ->
      begin
        match out with
        | [] -> S.Out
        | _  -> raise Unrecognized_expression
      end

  | X.List (X.Atom "quote" :: quote) ->
      begin
        match quote with
        | [term] -> recognize_expression @@ recognize_quote @@ term
        | _      -> raise Unrecognized_expression
      end

  | X.List (X.Atom "using" :: using) ->
      begin
        match using with
        | [X.Atom ns; X.List names; expr] ->
            let (ns', names') = recognize_using ns names  in
            let expr'         = recognize_expression expr in
            S.Using_expression (ns', names', expr')
        | _ -> raise Unrecognized_expression
      end

  | X.List (X.Atom callee :: args) ->
      let callee' = recognize_global callee in
      let args' = List.map recognize_expression args in
      S.Apply (callee', args')

  | X.Dict pairs ->
      let recognize_pair (k, v) = ( recognize_expression k
                                  , recognize_expression v ) in
      let pairs' = List.map recognize_pair pairs in

      let empty           = `Qualified (["std"; "dicts"], "empty") in
      let insert          = `Qualified (["std"; "dicts"], "insert") in
      let empty           = S.Apply (empty, []) in
      let insert d (k, v) = S.Apply (insert, [d; k; v]) in

      List.fold_left insert empty pairs'

  | _ ->
      raise Unrecognized_expression

and recognize_quote = function
  | X.Atom name ->
      X.List [X.Atom "std/->atom"; X.Text name]

  | X.Bool bool ->
      X.Bool bool

  | X.Text text ->
      X.Text text

  | X.List list ->
      let cons hd tl = X.List [X.Atom "std/lists/cons"; hd; tl] in
      let nil        = X.List [X.Atom "std/lists/nil"] in
      List.fold_right cons (List.map recognize_quote list) nil

  | X.Dict pairs ->
      let recognize_pair (k, v) = ( recognize_quote k
                                  , recognize_quote v ) in
      let pairs' = List.map recognize_pair pairs in
      X.Dict pairs'

and recognize_using ns names =
  let ns'    = String.split_on_char '/' ns in
  let names' = List.map (function | X.Atom n -> n
                                  | _ -> raise Unrecognized_definition)
                        names in
  (ns', names')
