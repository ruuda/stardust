(** Turn a program into an ECMAScript program. *)

(**
 * Thrown when an ECMAScript implementation is missing for a defforeign.
 *)
exception Missing_implementation of string

module Ecmascript : sig
  type statement
  type expression
  val format_statement : Sd_io.writer -> statement -> unit
  val format_expression : Sd_io.writer -> int -> expression -> unit
end

val gen_translation_unit
  :  Sd_name.qglobal Sd_syntax.translation_unit
  -> Sd_collect_callees.Callee_set.t
  -> Ecmascript.statement list

val gen_definition
  :  string list
  -> Sd_name.qglobal Sd_syntax.definition
  -> Ecmascript.statement list

val gen_contract
  :  Sd_name.qglobal Sd_syntax.contract
  -> Ecmascript.statement list
  -> Ecmascript.statement list

val gen_expression
  :  Sd_name.qglobal Sd_syntax.expression
  -> Ecmascript.expression
