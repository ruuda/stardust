module N = Sd_name
module S = Sd_syntax

(**
 * Map from names to fully-qualified names as imported with [using].
 *)
module Using_map = struct
  module M = Map.Make (String)

  type t = N.qglobal M.t

  let empty = M.empty

  let add = M.add

  let find_opt k m =
    try Some (M.find k m) with
    | Not_found -> None

  let collect ns names us =
    let step acc n = add n (`Qualified (ns, n)) acc in
    List.fold_left step us names
end

let qualify_global ns us = function
  | `Qualified (namespace, name) ->
      `Qualified (namespace, name)
  | `Unqualified name ->
      match Using_map.find_opt name us with
      | Some qglobal -> qglobal
      | None         -> `Qualified (ns, name)

let rec qualify_translation_unit tu =
  List.map (qualify_definition [] Using_map.empty) tu

and qualify_definition ns us = function
  | S.Using_definition (using_ns, names, ds) ->
      let us' = Using_map.collect using_ns names us in
      let ds' = List.map (qualify_definition ns us') ds in
      S.Using_definition (ns, names, ds')

  | S.Namespace (name, ds) ->
      let ns' = ns @ [name] in
      let ds' = List.map (qualify_definition ns' us) ds in
      S.Namespace (name, ds')

  | S.Defsub (name, params, contract, body) ->
      let contract' = qualify_contract ns us contract in
      let body' = qualify_expression ns us body in
      S.Defsub (name, params, contract', body')

  | S.Defforeign (name, params, contract, impls) ->
      let contract' = qualify_contract ns us contract in
      S.Defforeign (name, params, contract', impls)

  | S.Defmeth (name, params) ->
      S.Defmeth (name, params)

  | S.Defimpl (meth, cls, params, body) ->
      let meth' = qualify_global ns us meth in
      let cls'  = qualify_global ns us cls in
      let body' = qualify_expression ns us body in
      S.Defimpl (meth', cls', params, body')

  | S.Docarticle (title, elems) ->
      S.Docarticle (title, elems)

and qualify_contract ns us contract =
  let qualify_condition (name, cond) = (name, qualify_expression ns us cond) in
  { S.requires = List.map qualify_condition contract.S.requires
  ; S.ensures  = List.map qualify_condition contract.S.ensures }

and qualify_expression ns us = function
  | S.Using_expression (using_ns, names, expr) ->
      let us' = Using_map.collect using_ns names us in
      let expr' = qualify_expression ns us' expr in
      S.Using_expression (ns, names, expr')

  | S.Variable name ->
      S.Variable name

  | S.Literal_bool bool ->
      S.Literal_bool bool

  | S.Literal_text text ->
      S.Literal_text text

  | S.Apply (callee, args) ->
      let callee' = qualify_global ns us callee in
      let args' = List.map (qualify_expression ns us) args in
      S.Apply (callee', args')

  | S.Call (callee, args) ->
      let callee' = qualify_expression ns us callee in
      let args' = List.map (qualify_expression ns us) args in
      S.Call (callee', args')

  | S.Closure (params, body) ->
      let body' = qualify_expression ns us body in
      S.Closure (params, body')

  | S.If (cond, if_true, if_false) ->
      let cond'     = qualify_expression ns us cond in
      let if_true'  = qualify_expression ns us if_true in
      let if_false' = qualify_expression ns us if_false in
      S.If (cond', if_true', if_false')

  | S.Out ->
      S.Out
