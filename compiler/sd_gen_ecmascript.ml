module C = Sd_collect_callees
module I = Sd_io
module N = Sd_name
module S = Sd_syntax
module T = Sd_target

exception Missing_implementation of string

module Ecmascript = struct
  type statement =
    | Var of string * expression
    | Expression of expression
    | Return of expression
    | Literal of string

  and expression =
    | Variable of string
    | Literal_boolean of bool
    | Literal_string of string
    | Object
    | Field of expression * string
    | Call of expression * expression list
    | Lambda of string list * statement list
    | Binary of expression * string * expression
    | Ternary of expression * expression * expression

  (** Given self and parent precedences, apply parentheses if necessary. *)
  let parens w s p f =
    begin
      begin if s < p then I.write_all w "(" end;
      f ();
      begin if s < p then I.write_all w ")" end;
    end

  (** Given a binary operator, return its precedence. *)
  let binary_precedence = function
    | "||" -> 5
    | "="  -> 3
    | _    -> 0

  let rec format_statement w = function
    | Var (name, init) ->
        begin
          I.write_all w "var ";
          I.write_all w name;
          I.write_all w " = ";
          format_expression w 0 init;
          I.write_all w ";\n";
        end

    | Expression expr ->
        begin
          format_expression w 0 expr;
          I.write_all w ";\n";
        end

    | Return value ->
        begin
          I.write_all w "return ";
          format_expression w 0 value;
          I.write_all w ";\n";
        end

    | Literal text ->
        I.write_all w text

  and format_expression w p = function
    | Variable name ->
        I.write_all w name

    | Literal_boolean bool ->
        I.write_all w (if bool then "true" else "false")

    | Literal_string string ->
        begin
          I.write_all w "\"";
          I.write_all w string;
          I.write_all w "\"";
        end

    | Object ->
        I.write_all w "({})"

    | Field (obj, field) ->
        parens w 19 p @@ fun () -> begin
          format_expression w 19 obj;
          I.write_all w "[\"";
          I.write_all w field;
          I.write_all w "\"]";
        end

    | Call (callee, args) ->
        parens w 19 p @@ fun () -> begin
          format_expression w 19 callee;
          I.write_all w "(";
          List.iteri (fun i arg -> begin
                        begin if i > 0 then I.write_all w ", " end;
                        format_expression w 2 arg;
                      end)
                     args;
          I.write_all w ")";
        end

    | Lambda (params, body) ->
        begin
          I.write_all w "(function(";
          List.iteri (fun i param -> begin
                        begin if i > 0 then I.write_all w ", " end;
                        I.write_all w param;
                      end)
                     params;
          I.write_all w ") {\n";
          List.iter (format_statement w) body;
          I.write_all w "})";
        end

    | Binary (left, op, right) ->
        let s = binary_precedence op in
        parens w s p @@ fun () -> begin
          format_expression w s left;
          I.write_all w " ";
          I.write_all w op;
          I.write_all w " ";
          format_expression w s right;
        end

    | Ternary (cond, if_true, if_false) ->
        parens w 4 p @@ fun () -> begin
          format_expression w 4 cond;
          I.write_all w " ? ";
          format_expression w 4 if_true;
          I.write_all w " : ";
          format_expression w 4 if_false;
        end
end

module E = Ecmascript

let (%%) a b = E.Field (a, b)

(** Generate an immediately-invoked function expression. *)
let iife names values body =
  E.Call (E.Lambda (names, body), values)

module Mangling : sig
  (**
   * Mangle a global, separating the namespace segments and name with slashes
   * and appending the arity if given.
   *)
  val global : int option -> string list -> string -> string

  (**
   * Like {!val:global}, but takes a {!type:N.qglobal}.
   *)
  val qglobal : int option -> N.qglobal -> string

  (**
   * Mangle a variable, so that it is a correct ECMAScript identifier.
   *)
  val variable : string -> string
end = struct
  let global p ns n =
    String.concat "/" (ns @ [n]) ^
      match p with Some p' -> "~" ^ string_of_int p' | None -> ""

  let qglobal p (`Qualified (ns, n)) =
    global p ns n

  let variable =
    let is_safe c = (c >= 'a' && c <= 'z') ||
                    (c >= 'A' && c <= 'Z') ||
                    c = '_' in
    let rec go acc = function
      | "" -> acc
      | cs ->
          let c = String.get cs 0 in
          let c' = if is_safe c then String.make 1 c
                                else "$" ^ string_of_int (Char.code c) in
          go (acc ^ c') (String.sub cs 1 (String.length cs - 1)) in
    go ""
end

module Constants = struct
  (**
   * The global Stardust object, [window.stardust]. This object contains the
   * runtime under [rt] and user-defined subroutines.
   *
   * The outermost IIFE supplies this under the variable [$].
   *)
  let stardust_object = E.Variable "$"

  (**
   * The runtime object, containing runtime facilities such as core data
   * structures and method bookkeeping.
   *)
  let stardust_rt_object = stardust_object %% "rt"

  (**
   * Name of the variable containing the return value of a subroutine. This
   * variable is read by postconditions.
   *)
  let out_variable = "$out"
end

(**
 * The functions in this module correspond to those in the runtime.
 *)
module Rt : sig
  val assert_ : E.expression -> E.expression -> E.statement
  val define_method : string -> E.expression
  val implement_method : string -> string -> E.expression -> E.statement
  val register_callee : int * N.qglobal -> E.statement
end = struct
  let assert_ name cond =
    let call = E.Call ( Constants.stardust_rt_object %% "assert"
                      , [name; cond] ) in
    E.Expression call

  let define_method name =
    E.Call ( Constants.stardust_rt_object %% "defineMethod"
           , [E.Literal_string name] )

  let implement_method name cls impl =
    let call = E.Call ( Constants.stardust_rt_object %% "implementMethod"
                      , [E.Literal_string name; E.Literal_string cls; impl] ) in
    E.Expression call

  let register_callee (nparams, qglobal) =
    let mangled = Mangling.qglobal (Some nparams) qglobal in
    let call = E.Call ( Constants.stardust_rt_object %% "registerCallee"
                      , [E.Literal_string mangled] ) in
    E.Expression call
end

let rec gen_translation_unit defs callees =
  let defs'    = List.concat @@ List.map (gen_definition []) defs in
  let globals  = [E.Variable "window"; E.Variable "window" %% "stardust"] in
  let callregs = List.map Rt.register_callee (C.Callee_set.elements callees) in
  [ E.Expression (E.Literal_string "use strict")
  ; E.Expression (iife ["$$"; "$"] globals (defs' @ callregs)) ]

and gen_definition ns = function
  | S.Using_definition (_, _, defs) ->
      List.concat @@ List.map (gen_definition ns) defs

  | S.Namespace (name, defs) ->
      let ns' = ns @ [name] in
      List.concat @@ List.map (gen_definition ns') defs

  | S.Defsub (name, params, contract, body) ->
      let name'   = Mangling.global (Some (List.length params)) ns name in
      let params' = List.map Mangling.variable params in
      let body'   = [E.Var (Constants.out_variable, gen_expression body)] in
      let init    = E.Lambda (params', gen_contract contract body') in
      [E.Expression (E.Binary (Constants.stardust_object %% name', "=", init))]

  | S.Defforeign (name, params, contract, impls) ->
      let name' = Mangling.global (Some (List.length params)) ns name in
      let params' = List.map Mangling.variable params in
      let impl = try List.assoc T.Ecmascript impls with
                 | Not_found -> raise (Missing_implementation name') in
      let init = E.Lambda (params', gen_contract contract [E.Literal impl]) in
      [E.Expression (E.Binary (Constants.stardust_object %% name', "=", init))]

  | S.Defmeth (name, params) ->
      let name' = Mangling.global (Some (List.length params)) ns name in
      let init = Rt.define_method name' in
      [E.Expression (E.Binary (Constants.stardust_object %% name', "=", init))]

  | S.Defimpl (name, cls, params, body) ->
      let name'   = Mangling.qglobal (Some (List.length params)) name in
      let cls'    = Mangling.qglobal None cls in
      let params' = List.map Mangling.variable params in
      let impl    = E.Lambda (params', [E.Return (gen_expression body)]) in
      [Rt.implement_method name' cls' impl]

  | S.Docarticle (_, _) ->
      []

and gen_contract contract body =
  let assert_ (name, cond) = Rt.assert_ (E.Literal_string name) (gen_expression cond) in
  let requires' = List.map assert_ @@ contract.S.requires in
  let ensures'  = List.map assert_ @@ contract.S.ensures  in
  requires' @ body @ ensures' @ [E.Return (E.Variable Constants.out_variable)]

and gen_expression = function
  | S.Using_expression (_, _, expr) ->
      gen_expression expr

  | S.Variable name ->
      E.Variable (Mangling.variable name)

  | S.Literal_bool bool ->
      E.Literal_boolean bool

  | S.Literal_text text ->
      E.Literal_string text

  | S.Apply (callee, args) ->
      let callee' = Mangling.qglobal (Some (List.length args)) callee in
      let args' = List.map gen_expression args in
      E.Call (Constants.stardust_object %% callee', args')

  | S.Call (callee, args) ->
      let callee' = gen_expression callee in
      let args' = List.map gen_expression args in
      E.Call (callee', args')

  | S.Closure (params, body) ->
      let params' = List.map Mangling.variable params in
      let body' = gen_expression body in
      (* TODO: Assert argument count. *)
      E.Lambda (params', [E.Return body'])

  | S.If (cond, if_true, if_false) ->
      let cond'     = gen_expression cond in
      let if_true'  = gen_expression if_true in
      let if_false' = gen_expression if_false in
      (* TODO: Assert condition is Boolean. *)
      E.Ternary (cond', if_true', if_false')

  | S.Out ->
      E.Variable Constants.out_variable
