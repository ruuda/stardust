(** Qualify unqualified references to globals. *)

module Using_map : sig
  type t
  val empty    : t
  val add      : string -> Sd_name.qglobal -> t -> t
  val find_opt : string -> t -> Sd_name.qglobal option
  val collect  : string list -> string list -> t -> t
end

val qualify_translation_unit
  :  Sd_name.uglobal Sd_syntax.translation_unit
  -> Sd_name.qglobal Sd_syntax.translation_unit

val qualify_definition
  :  string list
  -> Using_map.t
  -> Sd_name.uglobal Sd_syntax.definition
  -> Sd_name.qglobal Sd_syntax.definition

val qualify_contract
  :  string list
  -> Using_map.t
  -> Sd_name.uglobal Sd_syntax.contract
  -> Sd_name.qglobal Sd_syntax.contract

val qualify_expression
  :  string list
  -> Using_map.t
  -> Sd_name.uglobal Sd_syntax.expression
  -> Sd_name.qglobal Sd_syntax.expression
